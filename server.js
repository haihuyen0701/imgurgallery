const express = require('express')

const port = process.env.PORT || 3001;

(async () => {
  const server = express()
  server.use(express.static(`${__dirname}/public`, { maxAge: 3600000 }))

  server.get('*', (req, res) => {
    res.sendFile(`${__dirname}/public/index.html`)
  })

  await server.listen(port)
})()
