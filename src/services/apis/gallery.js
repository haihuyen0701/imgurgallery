import endpoint from './config/endpoint'
import request from './config/request'

export const getList = (params) => request.get(endpoint.GET_LIST_GALLERY(params))
