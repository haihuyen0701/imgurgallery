import axios from 'axios'
import baseURL from './url'

const CLIENT_ID = '0b54b64502747e1'

const axiosRequest = axios.create({
  baseURL: baseURL(),
  timeout: 60 * 1000,
  headers: { authorization: `Client-ID ${CLIENT_ID}` },
})

axiosRequest.interceptors.response.use(async (response) => {
  if (response.data) {
    return response.data
  }
  return response
})

export default axiosRequest
