export default {
  GET_LIST_GALLERY: ({
    section,
    sort,
    window,
    page,
    showViral,
    showMature,
    albumPreviews,
  }) => `/gallery/${section}/${sort}/${window}/${page}?showViral=${showViral}&mature=${showMature}&album_previews=${albumPreviews}`,
}
