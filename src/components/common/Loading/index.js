import React from 'react'
import PropTypes from 'prop-types'
import { CircularProgress, withStyles } from '@material-ui/core'

const style = () => ({
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10,
    maxHeight: 600,
  },
})

const DataLoading = ({ loading, classes }) => loading && (
<div className={classes.container}>
  <CircularProgress />
</div>
)

DataLoading.defaultProps = {
  loading: false,
}

DataLoading.propTypes = {
  loading: PropTypes.bool,
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(DataLoading)
