import React from 'react'
import PropTypes from 'prop-types'
import {
  MenuItem,
  Select as MSelect,
  Typography,
  withStyles,
} from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/KeyboardArrowDown'
import clsx from 'clsx'
import COLORS from '~/themes/colors'

const style = (theme) => ({
  container: {
    width: '100%',
  },
  input: {
    width: '100%',
    background: COLORS.palette.secondaryBlue3,
    '&> .MuiMenu-paper': {
      marginTop: theme.spacing(1),
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      boxShadow: '0px 2px 10px #D6DEF2',
      borderRadius: 5,
    },
  },
  icon: {
    color: COLORS.palette.primaryBlue,
  },
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    background: COLORS.palette.secondaryBlue3,
    minHeight: 'unset',
    height: 16,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    '&:focus': {
      background: COLORS.palette.secondaryBlue3,
      border: 'none',
    },
  },
  pinLabel: {
    marginBottom: 5,
  },
  item: {
    justifyContent: 'space-between',
    borderRadius: 3,
  },
  title: {
    borderTop: `1px solid ${COLORS.gray.disableGray1}`,
    paddingTop: theme.spacing(1),
    paddingBottom: 1,
    marginTop: 5,
    '&.no0': {
      border: 'none',
      paddingTop: 1,
    },
  },
  subLabel: {
    color: COLORS.gray.textGray2,
  },
})

const Select = ({
  classes,
  value,
  options,
  className,
  pinLabel,
  onChange,
  ...props
}) => (
  <div className={clsx(classes.container, className)}>
    {pinLabel && (
      <Typography variant="subtitle1" className={classes.pinLabel}>
        {pinLabel}
      </Typography>
    )}
    <MSelect
      {...props}
      className={classes.input}
      classes={{
        root: classes.root,
        iconOutlined: classes.icon,
      }}
      value={value}
      IconComponent={ArrowDropDown}
      variant="outlined"
      onChange={(e) => e && e.target && e.target.value && onChange(e.target.value)}
    >
      {options.map((item, index) => item.title ? (
        <Typography
          key={item.value}
          variant="subtitle2"
          className={clsx(classes.title, `no${index}`)}
        >
          {item.label}
        </Typography>
      ) : (
        <MenuItem
          key={item.value}
          value={item.value}
          className={classes.item}
        >
          <div>{item.label}</div>
          {item.subLabel && (
          <div className={classes.subLabel}>{item.subLabel}</div>
          )}
        </MenuItem>
      ))}
    </MSelect>
  </div>
)

Select.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  options: PropTypes.arrayOf(PropTypes.shape()),
  pinLabel: PropTypes.string,
  onChange: PropTypes.func,
}

Select.defaultProps = {
  className: '',
  value: '',
  options: [],
  pinLabel: '',
  onChange: () => {},
}

export default withStyles(style)(Select)
