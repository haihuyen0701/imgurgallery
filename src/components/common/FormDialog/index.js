import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogActions from '@material-ui/core/DialogActions'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import Typography from '@material-ui/core/Typography'

import PropTypes from 'prop-types'
import COLORS from '~/themes/colors'

const DialogTitle = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1, 2),
  },
  closeButton: {
    position: 'absolute',
    top: 18,
    right: 14,
    color: COLORS.gray.darkGray,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    minHeight: 44,
  },
  titleText: {
    fontSize: 16,
    color: COLORS.palette.primaryBlue,
    marginRight: theme.spacing(3),
    fontWeight: 500,
  },
}))((props) => {
  const {
    children, extraHeader, classes, onClose, disableXClose, ...other
  } = props
  return (
    <MuiDialogTitle disableTypography className={`${classes.root} ${classes.header}`} {...other}>
      <Typography variant="h6" className={classes.titleText}>{children}</Typography>
      {extraHeader}
      {onClose && !disableXClose ? (
        <IconButton aria-label="close" size="small" className={classes.closeButton} onClick={onClose}>
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles(() => ({
  root: {
    padding: 0,
  },
}))(MuiDialogContent)

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1, 2),
    justifyContent: 'unset',
    minHeight: 61,
  },
}))(MuiDialogActions)

const CustomizedDialogs = ({
  open,
  onClose,
  title,
  extraHeader,
  children,
  footer,
  disableXClose,
  ...props
}) => {
  const handleClose = () => {
    onClose()
  }

  return (
    <Dialog
      open={open}
      maxWidth="sm"
      fullWidth
      disableBackdropClick
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      {...props}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={handleClose}
        disableXClose={disableXClose}
        extraHeader={extraHeader}
      >
        {title}
      </DialogTitle>
      <DialogContent dividers>
        {children}
      </DialogContent>
      <DialogActions>
        {footer()}
      </DialogActions>
    </Dialog>
  )
}

CustomizedDialogs.defaultProps = {
  extraHeader: '',
  onClose: () => {},
  disableXClose: false,
  footer: null,
}

CustomizedDialogs.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  disableXClose: PropTypes.bool,
  title: PropTypes.string.isRequired,
  extraHeader: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]).isRequired,
  footer: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node,
  ]),
}

export default CustomizedDialogs
