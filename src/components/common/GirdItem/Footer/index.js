import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import COLORS from '~/themes/colors'

const style = (theme) => ({
  divider: {
    width: `calc(100% + ${theme.spacing(4)}px)`,
    margin: theme.spacing(3, -3),
    marginTop: 'auto',
    height: 1,
    background: COLORS.gray.disableGray1,
  },
})

const Footer = ({ classes, children }) => (
  <>
    <div className={classes.divider} />
    <div className={classes.footer}>{children}</div>
  </>
)

Footer.defaultProps = {

}

Footer.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
}

export default withStyles(style)(Footer)
