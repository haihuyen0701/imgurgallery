import React from 'react'
import PropTypes from 'prop-types'
import { Grid, withStyles, Typography } from '@material-ui/core'
import clsx from 'clsx'
import COLORS from '~/themes/colors'
import Footer from './Footer'

const style = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    background: 'white',
    borderRadius: 10,
    marginBottom: theme.spacing(2),
    boxShadow: '0px 2px 10px #EDF1FB',
    padding: theme.spacing(3),
  },
  content: {
    flex: 1,
  },
  divider: {
    width: `calc(100% + ${theme.spacing(4)}px)`,
    margin: theme.spacing(3, -3),
    height: 1,
    background: COLORS.gray.disableGray1,
  },
  title: {
    color: COLORS.palette.primaryBlue,
    textTransform: 'uppercase',
    fontWeight: '500',
    marginBottom: theme.spacing(1),
  },
})

const GridItem = ({
  classes, className, children, title, titleClassName, header, ...props
}) => (
  <Grid item {...props} className={clsx(classes.container, className)}>
    {title && (<Typography variant="subtitle1" className={clsx(classes.title, titleClassName)}>{title}</Typography>)}
    {header}
    {<div className={classes.content}>{children}</div>}
  </Grid>
)

GridItem.defaultProps = {
  className: '',
  titleClassName: '',
  header: '',
  title: null,
}

GridItem.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  title: PropTypes.string,
  header: PropTypes.node,
  titleClassName: PropTypes.string,
}

GridItem.Footer = Footer

export default withStyles(style)(GridItem)
