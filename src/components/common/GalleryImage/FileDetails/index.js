import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import clsx from 'clsx'
import FileInfo from './FileInfo'
import style from './style'

const FileDetails = (props) => {
  const {
    classes,
    fileSelect,
    backFile,
    forwardFile,
    fileSelectIdx,
    mediaFiles,
  } = props

  const renderGalleryContent = () => (
    <div className={classes.galleryMainContent}>
      <div
        className={classes.forwardButton}
        role="presentation"
        onClick={backFile}
      >
        <ArrowBackIosIcon
          fontSize="large"
          className={clsx(classes.iconPrev, fileSelectIdx === 0 && 'disabled')}
        />
      </div>
      <div className={classes.galleryPreviewFileContainer}>
        <img
          id="main-image"
          className={classes.galleryPreviewFile}
          src={fileSelect.link ? fileSelect.link : null}
          alt="gallery"
        />
      </div>
      <div
        className={classes.forwardButton}
        role="presentation"
        onClick={forwardFile}
      >
        <ArrowForwardIosIcon
          fontSize="large"
          className={clsx(classes.iconNext, fileSelectIdx === mediaFiles.length - 1 && 'disabled')}
        />
      </div>
    </div>
  )
  return (
    <div className={classes.galleryContainer}>
      {renderGalleryContent()}
      <div className={classes.galleryInfoContainer}>
        <FileInfo file={fileSelect} />
      </div>
    </div>
  )
}

FileDetails.propTypes = {
  classes: PropTypes.shape().isRequired,
  fileSelect: PropTypes.oneOfType([PropTypes.object, PropTypes.shape]).isRequired,
  backFile: PropTypes.func.isRequired,
  forwardFile: PropTypes.func.isRequired,
  mediaFiles: PropTypes.oneOfType([PropTypes.array, PropTypes.shape]).isRequired,
  fileSelectIdx: PropTypes.number.isRequired,
}

export default withStyles(style)(FileDetails)
