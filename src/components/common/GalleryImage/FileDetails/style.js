import COLORS from '~/themes/colors'

const style = () => ({
  galleryContainer: {
    display: 'flex',
    flex: 1,
    height: 'calc(95vh - 200px)',
  },
  galleryInfoContainer: {
    width: 250,
    background: '#F8F9FF',
    padding: 30,
    paddingBottom: 0,
    borderLeftWidth: 1,
    borderLeftStyle: 'solid',
    borderLeftColor: '#EAEAEA',
    display: 'flex',
    flexDirection: 'column',
  },
  galleryMainContent: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  galleryPreviewFileContainer: {
    border: '1px solid #B4BFD9',
    borderRadius: 3,
    width: 650,
    height: 400,
    overflow: 'hidden',
  },
  galleryPreviewFile: {
    width: '100%',
    height: '100%',
    borderRadius: 3,
    position: 'relative',
    left: '50%',
    top: '50%',
    objectFit: 'contain',
    WebkitTransform: 'translate(-50%, -50%)',
    MozTransform: 'translate(-50%, -50%)',
    MsTransform: 'translate(-50%, -50%)',
    OTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
  forwardButton: {
    width: 50,
    height: 50,
    borderRadius: '50%',
    background: '#EDF1FB',
    color: COLORS.gray.darkGray,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 30,
    cursor: 'pointer',
  },
  iconPrev: {
    marginLeft: 15,
    '&.disabled': {
      color: COLORS.gray.disableGray1,
    },
  },
  iconNext: {
    '&.disabled': {
      color: COLORS.gray.disableGray1,
    },
  },

})

export default style
