import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import style from './style'

const FileInfo = (props) => {
  const {
    classes,
    file,
  } = props
  return (
    <>
      <span className={classes.galleryInfoLabel}>Title:</span>
      <span className={classes.galleryInfoValue}>
        {file.title ? file.title : 'The image dont have any title'}
      </span>

      <span className={classes.galleryInfoLabel}>Description</span>
      <span className={classes.galleryInfoValue}>
        {file.description
          ? file.description
          : 'The image dont have any description'}
      </span>

      <span className={classes.galleryInfoLabel}>Up votes:</span>
      <span className={classes.galleryInfoValue}>
        {' '}
        {file.ups ? file.ups : 'No upvotes information'}
      </span>

      <span className={classes.galleryInfoLabel}>Down votes:</span>
      <span className={classes.galleryInfoValue}>
        {file.downs ? file.downs : 'No downvotes information'}
      </span>

      <span className={classes.galleryInfoLabel}>Score:</span>
      <span className={classes.galleryInfoValue}>
        {file.score ? file.score : 'No score information'}
      </span>
      <span className={classes.galleryInfoLabel}>Views:</span>
      <span className={classes.galleryInfoValue}>
        {file.views ? file.views : 'No views information'}
      </span>
    </>
  )
}

FileInfo.propTypes = {
  classes: PropTypes.shape().isRequired,
  file: PropTypes.oneOfType([PropTypes.object, PropTypes.shape()]).isRequired,
}

export default withStyles(style)(FileInfo)
