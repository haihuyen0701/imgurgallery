const style = () => ({
  galleryInfoLabel: {
    fontWeight: 500,
    fontSize: 14,
    color: '#3B4757',
    marginTop: 20,
  },
  galleryInfoValue: {
    fontSize: 14,
    color: '#3B4757',
    marginTop: 10,
  },
})

export default style
