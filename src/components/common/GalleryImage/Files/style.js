import COLORS from '~/themes/colors'

const style = () => ({
  contentContainer: {
    padding: 20,
    paddingLeft: 30,
  },
  container: {
    marginTop: 20,
    minHeight: 100,
  },
  headerContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    '& .title': {
      fontSize: 18,
    },
  },
  filesContainer: {
    position: 'relative',
    cursor: 'pointer',
    width: '100%',
    paddingTop: '100%',
    border: '1px solid #B4BFD9',
    borderRadius: 3,
    '&:hover $fileButton': {
      display: 'flex',
    },

    '&.selected': {
      border: `2px solid ${COLORS.palette.primaryBlue}`,
    },
  },
  imageContainer: {
    position: 'absolute',
    borderRadius: 3,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  },
  editButtonsContainer: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'space-between',
    width: 'calc(100% - 10px)',
    bottom: '30px',
    left: 5,
  },
  fileButton: {
    display: 'none',
    cursor: 'pointer',
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
    borderRadius: 3,
    boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.12)',
    position: 'absolute',
    '& .search': {
      background: COLORS.palette.primaryBlue,
      color: 'white',
      borderRadius: 2,
    },
  },
  selectButton: {
    marginRight: 7,
  },
  formLabel: {
    textAlign: 'left',
    fontSize: 14,
    letterSpacing: 0,
    color: COLORS.palette.black,
    opacity: 1,
    padding: '4px 0 0 0',
    marginBottom: 15,
  },
  containerPagination: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
})

export default style
