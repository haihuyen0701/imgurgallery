import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Grid, FormLabel, withStyles } from '@material-ui/core'
import LazyLoad from 'react-lazyload'
import clsx from 'clsx'
import { Search } from '@material-ui/icons'
import style from './style'
import Select from '~/components/common/Select'
import { SELECT_BY_SECTION, SORT_BY_WINDOW } from '~/constants/homepage'
import { useHomePage } from '~/redux/hooks/homepage'
import SelectButton from '~/components/common/SelectButton'
import ControlPagination from '~/components/common/Pagination'

const Files = ({
  classes,
  files,
  openFile,
  fileSelected,
  setFileSelected,
  param,
}) => {
  const { actions } = useHomePage()
  const [valueSelect, setValueSelect] = useState(param.section)
  const [valueSortWindow, setValueSortWindow] = useState(param.window)
  const [viral, setValueViral] = useState(param.showViral && param.section === 'user' ? 'viral' : 'no-viral')
  const [page, setPage] = useState(1)
  const page_size = 100
  const totalPage = parseInt(files.length / page_size, 10)
  const isFileSelected = (file) => fileSelected
    && fileSelected.some((fileSelect) => fileSelect.link === file.link)
  const handleSelectFile = (file, fileIdx) => {
    if (!fileSelected) return openFile(fileIdx)
    return setFileSelected(isFileSelected(file) ? [] : [file])
  }

  const handleSelect = (e) => {
    setValueSelect(e)
    const newParams = { ...param, section: e }
    actions.getGallery(newParams)
  }

  const handleShowVirual = (e) => {
    if (e === 'viral') {
      const newParams = { ...param, section: 'user', showViral: true }
      actions.getGallery(newParams, setValueViral(e), () => {})
    } else {
      const newParams = { ...param, showViral: false }
      actions.getGallery(newParams, setValueViral(e), () => {})
    }
    setValueViral(e)
  }

  const handleSortByWindow = (e) => {
    const newParams = { ...param, window: e }
    actions.getGallery(newParams)
    setValueSortWindow(e)
  }

  const handleChangePage = (_, value) => {
    setPage(value)
  }

  const renderFiles = () => files
    .slice(page * page_size - page_size, page * page_size)
    .map((file, index) => file.link.substr(file.link.lastIndexOf('.')) !== '.mp4' ? (
      <Grid key={`file-${index}`} item xs={4} md={3} lg={2} xl={1}>
        <LazyLoad>
          <div
            role="presentation"
            className={clsx(classes.filesContainer)}
            onClick={() => handleSelectFile(file, index)}
          >
            <div className={classes.imageContainer}>
              <img
                className={classes.image}
                alt={`img-${file.id}`}
                src={file.link}
              />
            </div>
            <div className={classes.editButtonsContainer}>
              <div
                role="presentation"
                className={classes.fileButton}
                onClick={() => {}}
              >
                <Search className="search" />
              </div>
            </div>
          </div>
        </LazyLoad>
      </Grid>
    ) : null)
  return (
    <div className={classes.contentContainer}>
      <div className={classes.headerContainer} />
      <Grid container spacing={2}>
        <Grid item xs={2}>
          <Select
            pinLabel="Selecting gallery section by: "
            options={SELECT_BY_SECTION}
            value={valueSelect}
            onChange={handleSelect}
          />
        </Grid>
        <Grid item xs={8}>
          <FormLabel component="legend" className={classes.formLabel}>
            Viral Image
          </FormLabel>
          <SelectButton
            buttons={[
              { label: 'Including ', value: 'viral' },
              { label: 'Excluding', value: 'no-viral' },
            ]}
            name="viral"
            onChange={handleShowVirual}
            buttonClassName={classes.selectButton}
            value={viral}
          />
        </Grid>
        <Grid item xs={2}>
          <Select
            pinLabel="Sort by: "
            options={SORT_BY_WINDOW}
            value={valueSortWindow}
            onChange={handleSortByWindow}
          />
        </Grid>
      </Grid>
      <Grid className={classes.container} container spacing={2}>
        {renderFiles()}
      </Grid>
      <Grid container className={classes.containerPagination}>
        <ControlPagination
          variant="outlined"
          shape="rounded"
          totalPage={totalPage}
          page_size={page_size}
          page={page}
          onChange={handleChangePage}
        />
      </Grid>
    </div>
  )
}

Files.defaultProps = {
  files: [],
  setFileSelected: () => {},
  param: {},
}

Files.propTypes = {
  classes: PropTypes.shape().isRequired,
  files: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]),
  fileSelected: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]),
  openFile: PropTypes.func.isRequired,
  setFileSelected: PropTypes.func,
  param: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]),
}

export default withStyles(style)(Files)
