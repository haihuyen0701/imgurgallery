import React from 'react'
import PropTypes from 'prop-types'
import { Button as ThemeButton, withStyles } from '@material-ui/core'
import clsx from 'clsx'
import CircularProgress from '@material-ui/core/CircularProgress'
import COLORS from '~/themes/colors'

const ButtonStyled = withStyles(() => ({
  root: {
    height: 32,
    textTransform: 'none',
    boxShadow: 'none',
    borderRadius: 5,
    '&.Mui-disabled': {
      color: `${COLORS.gray.disableGray} !important`,
      backgroundColor: `${COLORS.gray.disableGray2} !important`,
    },
  },
  label: {
    fontSize: 14,
  },
}))(ThemeButton)

const CircularProgressStyled = withStyles(() => ({
  root: {
    marginLeft: 8,
  },
}))(CircularProgress)

const style = () => ({
  rounded: {
    height: 38,
    borderRadius: 19,
  },
  // label
  labelPrimary: {
    color: `${COLORS.palette.white} !important`,
  },
  labelSecondary: {
    color: COLORS.gray.darkGray,
  },
  // bg
  bgPrimary: {
    background: `${COLORS.palette.primaryBlue} !important`,
    '&:hover': {
      boxShadow: '0px 2px 6px #2933C533',
      background: COLORS.palette.primaryBlue,
    },
  },
  bgSecondary: {
    background: COLORS.palette.secondaryBlue3,
    '&:hover': {
      background: COLORS.palette.secondaryBlue3,
      boxShadow: '0px 2px 6px #2933C533',
    },
  },
  // variant
  outline: {
    background: COLORS.palette.secondaryBlue3,
    color: COLORS.palette.primaryBlue,
    border: `1px solid ${COLORS.palette.primaryBlue}`,
    '&:hover': {
      boxShadow: '0px 2px 6px #2933C533',
      background: COLORS.palette.secondaryBlue3,
    },
  },
  loading: {
    opacity: 0.4,
    boxShadow: 'none',
    pointerEvents: 'none',
  },
})

const Button = ({
  classes,
  className,
  label,
  children,
  color,
  loading,
  rounded,
  variant,
  customClasses,
  ...props
}) => {
  const labelColor = color === 'primary' ? `${classes.labelPrimary}` : `${classes.labelSecondary}`
  const backgroundColor = color === 'primary' ? `${classes.bgPrimary}` : `${classes.bgSecondary}`
  const hasRounded = rounded ? `${classes.rounded}` : ''
  const outline = variant === 'outlined' ? classes.outline : ''
  const loadingClasses = loading ? classes.loading : ''

  return (
    <ButtonStyled
      className={
        clsx(
          hasRounded,
          labelColor,
          backgroundColor,
          outline,
          loadingClasses,
          className,
        )
      }
      classes={{
        root: classes.root,
        ...customClasses,
      }}
      variant={variant}
      {...props}
    >
      {label || children}
      {loading && (
      <CircularProgressStyled
        size={18}
        color="inherit"
      />
      )}
    </ButtonStyled>
  )
}

Button.defaultProps = {
  label: '',
  color: 'primary',
  variant: 'contained',
  children: '',
  loading: false,
  rounded: false,
  className: '',
  customClasses: {},
}

Button.propTypes = {
  classes: PropTypes.shape().isRequired,
  customClasses: PropTypes.shape(),
  className: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.any,
  ]),
  label: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.string,
    PropTypes.any,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.string,
  ]),
  color: PropTypes.string,
  loading: PropTypes.bool,
  rounded: PropTypes.bool,
  variant: PropTypes.string,

}

export default withStyles(style)(Button)
