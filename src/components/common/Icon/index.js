import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import style from './style'
import colors from '~/themes/colors'

const Icon = ({
  classes, name, size, color, className,
}) => (
  <div
    className={clsx(classes.icon, size, className)}
    style={{
      color,
      maskImage: `url(/icons/${name}.svg)`,
      WebkitMaskImage: `url(/icons/${name}.svg)`,
      backgroundColor: color,
    }}
  />
)

Icon.defaultProps = {
  size: 'medium',
  color: colors.gray.darkGray,
  className: '',
}

Icon.propTypes = {
  classes: PropTypes.shape(),
  name: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  color: PropTypes.string,
  className: PropTypes.string,
}

export default withStyles(style)(Icon)
