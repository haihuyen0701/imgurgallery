import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import Button from '../Button'
import COLORS from '~/themes/colors'

const style = () => ({
  root: {
    marginRight: 20,
    fontWeight: 400,
  },
  disabled: {
    background: COLORS.gray.disableGray2,
    color: `${COLORS.palette.black} !important`,
    border: '1px solid transparent',
  },
})
const SelectButton = ({
  classes, buttons, onChange, value, buttonClassName,
}) => buttons.map((button) => (
  <Button
    className={clsx(
      classes.root,
      value !== button.value && classes.disabled,
      buttonClassName,
    )}
    key={button.value}
    label={button.label}
    color="secondary"
    variant="outlined"
    onClick={() => onChange(button.value)}
  />
))
SelectButton.defaultProps = {
  buttons: [],
  onChange: () => {},
  buttonClassName: '',
}

SelectButton.propTypes = {
  classes: PropTypes.shape().isRequired,
  buttons: PropTypes.oneOfType([PropTypes.shape(), PropTypes.array]),
  onChange: PropTypes.func,
  buttonClassName: PropTypes.string,
}

export default withStyles(style)(SelectButton)
