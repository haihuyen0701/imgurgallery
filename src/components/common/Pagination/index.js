import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Pagination from '@material-ui/lab/Pagination'
import clsx from 'clsx'
import COLORS from '~/themes/colors'

const style = () => ({
  root: {
    marginTop: 30,
    fontWeight: 400,
    ' & .MuiPaginationItem-outlined': {
      color: `${COLORS.palette.primaryBlue}`,
      border: `1px solid ${COLORS.palette.primaryBlue}`,
      background: '#EDF1FB',
    },
  },
})

const ControlPagination = ({
  classes,
  className,
  page,
  page_size,
  totalPage,
  handleChange,
  ...props
}) => (
  <div className={clsx(classes.root, className)}>
    <Pagination
      count={totalPage}
      page={page}
      {...props}
    />
  </div>
)

ControlPagination.defaultProps = {
  handleChange: () => {},
  className: '',
  page: 1,
  page_size: 20,
  totalPage: 20,
}

ControlPagination.propTypes = {
  classes: PropTypes.shape().isRequired,
  handleChange: PropTypes.func,
  className: PropTypes.string,
  page: PropTypes.number,
  page_size: PropTypes.number,
  totalPage: PropTypes.number,
}

export default withStyles(style)(ControlPagination)
