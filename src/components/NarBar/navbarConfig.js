export default [
  {
    pages: [
      {
        title: 'HomePage',
        href: '/',
        icon: 'dashboard',
      },
      {
        title: 'About You',
        href: '/aboutyou',
        icon: 'box',
        children: [
          {
            title: 'For You',
            href: '/aboutyou/foryou',
          },
          {
            title: 'Following',
            href: '/aboutyou/following',
          },
        ],
      },
    ],
  },
]
