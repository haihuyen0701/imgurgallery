import React from 'react'
import PropTypes from 'prop-types'
import { Paper, withStyles } from '@material-ui/core'
import Navigation from '../Navigation'
import navbarConfig from './navbarConfig'
import color from '~/themes/colors'

const style = (theme) => ({
  rootFull: {
    background: color.palette.primaryBlue,
    position: 'absolute',
    top: 0,
    bottom: 0,
    zIndex: 4,
    width: 80,
    visibility: 'hidden',
    height: '100%',
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      display: 'none',
    },

    '-webkit-transition': 'all 0.3s ease',
    '-moz-transition': 'all 0.3s ease',
    '-o-transition': 'all 0.3s ease',
    '-ms-transition': 'all 0.3s ease',
    transition: 'all 0.3s ease',
    '& .navbar-title': {
      opacity: 0,
    },
    '& .navbar-dot': {
      opacity: 0,
    },
    '&:hover': {
      visibility: 'visible',
      width: 256,
      '& .navbar-title': {
        opacity: 1,
        marginLeft: theme.spacing(1),
      },
      '& .navbar-dot': {
        opacity: 1,
        marginLeft: -21,
      },
    },
  },
  root: {
    background: color.palette.primaryBlue,
    height: '1000px',
    zIndex: 3,
    flex: '0 0 auto',
    width: 80,
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      display: 'none',
    },
    '&:hover': {
      '&+$rootFull': {
        visibility: 'visible',
        width: 256,
        '& .navbar-title': {
          opacity: 1,
          marginLeft: theme.spacing(1),
        },
        '& .navbar-dot': {
          opacity: 1,
          marginLeft: -21,
        },
      },
    },
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2),
  },
})

const renderNavBar = (location, isFull) => (
  <nav>
    {navbarConfig.map(({ title, pages }) => (
      <Navigation
        key={title}
        pages={pages}
        title={title}
        router={location}
        data={{}}
        isFull={isFull}
      />
    ))}
  </nav>
)

const NavBar = ({ classes, location }) => (
  <>
    <Paper className={classes.root} square>
      <div className={classes.content}>{renderNavBar(location, false)}</div>
    </Paper>
    <Paper className={classes.rootFull} square>
      <div className={classes.content}>{renderNavBar(location, true)}</div>
    </Paper>
  </>
)

NavBar.defaultProps = {}

NavBar.propTypes = {
  classes: PropTypes.shape().isRequired,
  location: PropTypes.shape().isRequired,
}

export default withStyles(style)(NavBar)
