import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import style from './style'
import Icon from '~/components/common/Icon'

const Topbar = ({ classes }) => (
  <div className={classes.container}>
    <div className={classes.item}>
      <div className={classes.logo}>
        <img
          src="/icons/logo_color_simple.svg"
          className={classes.logoIcon}
          alt="logo"
        />
        <img
          src="/icons/logo_color_text.svg"
          className={classes.logoText}
          alt="logo"
        />
      </div>
      <div className={classes.store}>
        <Icon name="store" className={classes.icon} />
      </div>
      <a href="https://imgur.com/" target="_" className={classes.store}>
        <Icon name="menu" className={classes.icon} />
        View Imgur Website
      </a>
    </div>
    <div className={classes.item}>
      <Icon name="alert" className={classes.icon} />
      <img src="/icons/user.svg" className={classes.avatar} alt="logo" />
    </div>
  </div>
)

Topbar.propTypes = {
  classes: PropTypes.shape(),
}

export default withStyles(style)(Topbar)
