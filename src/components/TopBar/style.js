import colors from '~/themes/colors'

const style = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 70,
    borderBottom: `1px solid ${colors.gray.disableGray1}`,
  },
  item: {
    display: 'flex',
    alignItems: 'center',
  },
  logo: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: theme.spacing(3),
    '& img': {},
  },
  logoIcon: {
    height: 32,
  },
  logoText: {
    marginLeft: theme.spacing(2),
    height: 20,
  },
  store: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(4),
    color: colors.gray.textGray1,
    fontSize: 14,
  },
  icon: {
    marginRight: theme.spacing(1),
  },
  dropdown: {
    width: 180,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(3),
    border: `1px solid ${colors.gray.grayBorder}`,
  },
})

export default style
