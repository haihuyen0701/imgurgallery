import React, { forwardRef } from 'react'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import {
  ListItem, Button, Collapse, makeStyles,
} from '@material-ui/core'
import { ExpandMore, ExpandLess } from '@material-ui/icons'
import { Link, matchPath } from 'react-router-dom'
import Icon from '~/components/common/Icon'
import colors from '~/themes/colors'

const CustomRouterLink = forwardRef(
  (
    {
      to, children, className, activeClassName, exact, active, ...props
    },
    ref,
  ) => (
    <Link
      to={to}
      ref={ref}
      style={{ flexGrow: 1 }}
      className={`${className} ${active ? activeClassName : ''}`}
      {...props}
    >
      {children}
    </Link>
  ),
)

CustomRouterLink.propTypes = {
  to: PropTypes.string.isRequired,
  exact: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  activeClassName: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  active: PropTypes.bool.isRequired,
}

const useStyles = makeStyles(() => ({
  item: {
    display: 'block',
    paddingTop: 0,
    paddingBottom: 0,
  },
  itemLeaf: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    color: 'white',
    padding: 8,
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    minWidth: 0,
    fontWeight: 400,
    minHeight: 48,
    '&:hover': {
      background: 'rgba(0,0,0,.1)',
    },
  },
  buttonLeaf: {
    position: 'relative',
    color: 'white',
    padding: 8,
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    minWidth: 0,
    fontWeight: 400,
    minHeight: 48,
    '&.depth-0': {
      fontWeight: 400,
    },
    '&:hover': {
      background: 'rgba(0,0,0,.1)',
    },
  },
  icon: {
    flex: '0 0 32px',
  },
  title: {
    marginLeft: -5,
    '-webkit-transition': 'all 0.2s ease',
    '-moz-transition': 'all 0.2s ease',
    '-o-transition': 'all 0.2s ease',
    '-ms-transition': 'all 0.2s ease',
    transition: 'all 0.2s ease',
    whiteSpace: 'nowrap',
  },
  expandIcon: {
    marginLeft: 'auto',
    height: 16,
    width: 16,
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 'auto',
    '&.withBadge': {
      marginLeft: 8,
    },
  },
  active: {
    background: 'white',
    color: colors.palette.primaryBlue,
    '&:hover': {
      background: 'white',
      color: colors.palette.primaryBlue,
    },
  },
  activeLeaf: {},
  badge: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#FF4949',
    color: 'white',
    marginLeft: 'auto',
    fontSize: 13,
    fontWeight: 500,
    width: 20,
    height: 20,
    borderRadius: 10,
    paddingBottom: 3,
  },
  dot: {
    position: 'absolute',
    width: 10,
    height: 10,
    background: 'white',
    borderRadius: 5,
    marginLeft: -16,
    marginBottom: 2,
    '-webkit-transition': 'all 0.2s ease',
    '-moz-transition': 'all 0.2s ease',
    '-o-transition': 'all 0.2s ease',
    '-ms-transition': 'all 0.2s ease',
    transition: 'all 0.2s ease',
  },
}))

const NavigationListItem = ({
  title,
  href,
  depth,
  children,
  icon,
  open,
  label: Label,
  pathname,
  onClick,
  data,
  isFull,
  handleToggle,
  parentHref,
  ...rest
}) => {
  const classes = useStyles()

  const style = {
    paddingLeft: depth > 0 ? 32 + 8 * depth : 8,
  }

  let active

  if (children) {
    active = !!matchPath(pathname, { path: parentHref, exact: false })
    return (
      <ListItem {...rest} className={classes.item} disableGutters>
        <Button
          activeClassName={classes.active}
          className={classes.button}
          onClick={() => handleToggle(title)}
          style={style}
          component={CustomRouterLink}
          active={active}
        >
          {icon && (
            <Icon
              className={classes.icon}
              name={icon}
              color={active ? colors.palette.primaryBlue : 'white'}
            />
          )}
          {isFull && (
            <>
              <div className={clsx(classes.title, 'navbar-title')}>{title}</div>
              {data && data !== 0 ? (
                <div className={classes.badge}>{data}</div>
              ) : (
                ''
              )}
              {open ? (
                <ExpandLess
                  className={`${classes.expandIcon}${
                    data && data !== 0 ? ' withBadge' : ''
                  }`}
                  color="inherit"
                />
              ) : (
                <ExpandMore
                  className={`${classes.expandIcon}${
                    data && data !== 0 ? ' withBadge' : ''
                  }`}
                  color="inherit"
                />
              )}
            </>
          )}
        </Button>
        <Collapse in={open}>{children}</Collapse>
      </ListItem>
    )
  }

  active = !!matchPath(pathname, { path: href, exact: true })
  return (
    <ListItem {...rest} className={classes.itemLeaf} disableGutters>
      <Button
        activeClassName={!icon ? classes.activeLeaf : classes.active}
        className={clsx(classes.buttonLeaf, `depth-${depth}`)}
        component={CustomRouterLink}
        exact
        style={style}
        to={href}
        onClick={onClick}
        active={active}
      >
        {icon ? (
          <Icon
            className={classes.icon}
            name={icon}
            color={active ? colors.palette.primaryBlue : 'white'}
          />
        ) : (
          active && <div className={clsx(classes.dot, 'navbar-dot')} />
        )}
        {isFull && (
          <>
            <div className={clsx(classes.title, 'navbar-title')}>{title}</div>
            {data && data !== 0 ? (
              <div className={classes.badge}>{data}</div>
            ) : (
              ''
            )}
            {Label && (
              <span
                className={`${classes.label}${
                  data && data !== 0 ? ' withBadge' : ''
                }`}
              >
                <Label />
              </span>
            )}
          </>
        )}
      </Button>
    </ListItem>
  )
}

NavigationListItem.propTypes = {
  children: PropTypes.node,
  depth: PropTypes.number,
  href: PropTypes.string,
  icon: PropTypes.shape(),
  label: PropTypes.shape(),
  open: PropTypes.bool,
  title: PropTypes.string.isRequired,
  pathname: PropTypes.string,
  onClick: PropTypes.func,
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isFull: PropTypes.bool,
  handleToggle: PropTypes.func.isRequired,
  parentHref: PropTypes.string,
}

NavigationListItem.defaultProps = {
  depth: 0,
  open: false,
  children: null,
  label: null,
  pathname: '',
  href: '#',
  onClick: () => {},
  data: '',
  isFull: false,
  parentHref: '',
  icon: null,
}

export default NavigationListItem
