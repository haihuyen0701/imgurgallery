import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { List, makeStyles, Typography } from '@material-ui/core'
import { matchPath } from 'react-router-dom'
import NavigationListItem from './NavigationListItem'
import { mapDataForNavbar } from '~/utils/router'

const reduceChildRoutes = ({
  router,
  items,
  page,
  depth,
  data,
  isFull,
  open,
  setOpen,
}) => {
  if (page.children) {
    items.push(
      <NavigationListItem
        depth={depth}
        icon={page.icon}
        key={page.title}
        label={page.label}
        open={open}
        title={page.title}
        pathname={router.pathname}
        data={data[mapDataForNavbar(page.href)]}
        isFull={isFull}
        handleToggle={setOpen}
        parentHref={page.href}
      >
        <NavigationList
          depth={depth + 1}
          pages={page.children}
          router={router}
          data={data}
          isFull={isFull}
        />
      </NavigationListItem>,
    )
  } else {
    items.push(
      <NavigationListItem
        depth={depth}
        href={page.href}
        icon={page.icon}
        key={page.title}
        label={page.label}
        title={page.title}
        pathname={router.pathname}
        data={data[mapDataForNavbar(page.href)]}
        isFull={isFull}
      />,
    )
  }
  return items
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(3),
  },
}))

const NavigationList = ({
  pages, isFull, router, open, setOpen, ...rest
}) => (
  <List>
    {pages.reduce(
      (items, page, index) => reduceChildRoutes({
        items,
        page,
        isFull,
        open: isFull && open ? open[index] : false,
        setOpen,
        router,
        ...rest,
      }),
      [],
    )}
  </List>
)

NavigationList.propTypes = {
  router: PropTypes.shape().isRequired,
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  depth: PropTypes.number.isRequired,
  isFull: PropTypes.bool.isRequired,
  pages: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}

const Navigation = ({
  title, pages, router, isFull, ...rest
}) => {
  const [open, setOpen] = useState(
    isFull
      ? pages.map(
        (item) => !!matchPath(router.pathname, {
          path: item.href,
          exact: false,
        }),
      )
      : [],
  )

  const handleToggle = (t) => {
    setOpen(
      pages.map((item, index) => (item.title === t ? !open[index] : false)),
    )
  }

  const classes = useStyles()
  return (
    <div {...rest} className={classes.root}>
      {title && <Typography variant="caption">{title}</Typography>}
      <NavigationList
        depth={0}
        pages={pages}
        router={router}
        data={{}}
        isFull={isFull}
        open={open}
        setOpen={handleToggle}
      />
    </div>
  )
}

Navigation.defaultProps = {
  isFull: false,
}

Navigation.propTypes = {
  pages: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  title: PropTypes.string.isRequired,
  router: PropTypes.shape().isRequired,
  isFull: PropTypes.bool,
}

export default Navigation
