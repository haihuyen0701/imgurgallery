import colors from './colors'

export default {
  MuiOutlinedInput: {
    input: {
      fontSize: 14,
      color: colors.palette.black,
      '&::placeholder': {
        fontSize: 14,
        color: colors.gray.textGray2,
      },
    },
    root: {
      background: colors.palette.secondaryBlue3,
    },
  },
}
