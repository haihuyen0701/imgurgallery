export default {
  fontFamily: 'Rubik',
  h1: {
    fontSize: 32,
    fontWeight: 500,
    paddingBottom: 16,
  },
}
