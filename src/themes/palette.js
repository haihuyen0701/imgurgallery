import colors from './colors'

export default {
  primary: {
    main: colors.palette.primaryBlue,
  },
  secondary: {
    main: colors.palette.secondaryBlue3,
    light: colors.palette.secondaryBlue2,
    contrastText: colors.palette.secondaryBlue2,
  },
  error: {
    main: colors.accent.rubyRed1,
    light: colors.accent.rubyRed2,
  },
  success: {
    main: colors.accent.viridianGreen1,
    light: colors.accent.viridianGreen2,
  },
  warning: {
    main: colors.accent.mustardYellow1,
    light: colors.accent.mustardYellow2,
  },
  textPrimary: {
    main: colors.palette.black,
  },
}
