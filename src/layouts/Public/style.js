const style = () => ({
  body: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: 'calc(100vh - 70px)',
  },
  content: {
    position: 'relative',
    width: 'calc(100vw - 80px)',
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    background: '#F8F9FF',
    height: '100%',
  },
})

export default style
