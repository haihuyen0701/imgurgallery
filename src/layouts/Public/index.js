import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import clsx from 'clsx'
import style from './style'
import Topbar from '~/components/TopBar'
import Navbar from '~/components/NarBar'

const PublicLayout = ({
  classes, children, location,
}) => (
  <>
    <Topbar />
    <div className={classes.body}>
      <Navbar location={location} />
      <div className={clsx(classes.content)}>{children}</div>
    </div>
  </>
)

PublicLayout.defaultProps = {
}

PublicLayout.propTypes = {
  classes: PropTypes.shape(),
  location: PropTypes.shape(),
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
}

export default withStyles(style)(withRouter(PublicLayout))
