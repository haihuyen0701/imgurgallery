import React, { Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/core/styles'
import { Provider } from 'react-redux'
import theme from './themes/theme'
import store from './redux/store'
import SystemRouter from './router/system'
import PublicLayout from './layouts/Public'
import '~/assets/css/style.scss'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Suspense fallback={<div>Loading....</div>}>
          <BrowserRouter>
            <Switch>
              <Route path="/">
                <PublicLayout>
                  {SystemRouter.filter(({ layout }) => layout === 'public').map(
                    (router) => (
                      <Route
                        key={router.path}
                        exact={router.exact}
                        path={router.path}
                        component={(props) => (
                          <router.component
                            {...props}
                            // eslint-disable-next-line react/prop-types
                            {...props.match.params}
                            {...router.xProps}
                          />
                        )}
                      />
                    ),
                  )}
                </PublicLayout>
              </Route>
            </Switch>
          </BrowserRouter>
        </Suspense>
      </Provider>
    </ThemeProvider>
  )
}

export default App
