import React, { useEffect, useState, useCallback } from 'react'
import { withStyles, Grid } from '@material-ui/core'
import PropTypes from 'prop-types'
import { useHomePage } from '~/redux/hooks/homepage'

const style = {
  container: {
    padding: 50,
    width: '(calc100% - 32px)',
    maxWidth: 1600,
    paddingLeft: 16,
    paddingRight: 16,
    boxSizing: 'unset',
  },
}

const HomePage = ({ classes }) => {
  const { galleryList, actions } = useHomePage()
  const { data: param } = galleryList

  useEffect(() => {
    actions.getGallery(param)
  }, [])

  return (
    <div className={classes.container}>
      <Grid container spacing={2} className="grid lg">
        <Grid item xs={12} />
      </Grid>
    </div>
  )
}
export default withStyles(style)(HomePage)

HomePage.defaultProps = {
}

HomePage.propTypes = {
  classes: PropTypes.shape().isRequired,
}
