const SideBarRouter = [
  {
    pages: [
      {
        title: 'HomePage',
        href: '/',
        icon: 'dashboard',
      },
    ],
  },
]

export default SideBarRouter
