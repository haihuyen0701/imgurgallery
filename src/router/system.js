import React from 'react'

const HomePage = React.lazy(() => import('../pages/HomePage'))

const SystemRouter = [
  {
    path: '/',
    component: HomePage,
    exact: true,
    layout: 'public',
  },
]

export default SystemRouter
