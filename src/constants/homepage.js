export const SELECT_BY_SECTION = [
  {
    value: 'hot',
    label: 'hot',
  },
  {
    value: 'top',
    label: 'top',
  },
  { value: 'user', label: 'user' },
]

export const SORT_BY_WINDOW = [
  {
    value: 'day',
    label: 'day',
  },
  {
    value: 'week',
    label: 'week',
  },
  { value: 'month', label: 'month' },
  { value: 'year', label: 'year' },
  { value: 'all', label: 'all' },
]
