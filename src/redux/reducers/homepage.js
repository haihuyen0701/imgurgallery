import { fromJS } from 'immutable'
import { HOME_PAGE_ACTIONS } from '../actions/homepage'

const initialState = {
  gallery: {
    data: {},
    loading: false,
    error: {},
    param: {
      section: 'hot',
      window: 'day',
      sort: 'viral',
      page: 1,
      showViral: true,
      showMature: false,
      albumPreviews: true,
    },
  },
}

const reducer = (state = fromJS(initialState), action) => {
  const { type, payload } = action
  switch (type) {
    case HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE:
      return state.setIn(['gallery', 'loading'], true)
    case HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE_OK:
      return state
        .setIn(['gallery', 'loading'], false)
        .setIn(['gallery', 'data'], fromJS(payload.data))
        .setIn(['gallery', 'param'], fromJS(payload.param))
    case HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE_ERR:
      return state
        .setIn(['gallery', 'loading'], false)
        .setIn(['gallery', 'data'], {})
        .setIn(['gallery', 'error'], fromJS(payload.error))

    default:
      return state
  }
}

export default reducer
