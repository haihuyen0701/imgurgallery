import { combineReducers } from 'redux-immutable'
import homepage from './homepage'

const rootReducer = combineReducers({
  homepage,
})

const reducer = (state, action) => rootReducer(
  state,
  action,
)

export default reducer
