import { createSelector } from 'reselect'

const getGallery = (state) => state.getIn(['homepage', 'gallery'])

export const getGallerySelector = createSelector(
  getGallery,
  (gallery) => gallery.toJS(),
)
