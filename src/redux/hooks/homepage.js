import { useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getGallerySelector } from '../selectors/homepage'
import homepageActions from '../actions/homepage'

export const useHomePage = () => {
  const galleryList = useSelector(getGallerySelector)

  const dispatch = useDispatch()
  const actions = useMemo(() => bindActionCreators(homepageActions, dispatch), [
    dispatch,
  ])
  return useMemo(
    () => ({
      actions,
      galleryList,
    }),
    [actions, galleryList],
  )
}
