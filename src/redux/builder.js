// eslint-disable-next-line import/prefer-default-export
export function apiAction(
  apiRequest,
  [GET, SUCCESS, ERROR],
  xProps = {},
  onSuccess,
  onError,
) {
  return (dispatch) => {
    dispatch({
      type: GET,
      payload: { ...xProps },
    })
    return apiRequest
      .then((response) => {
        dispatch({
          type: SUCCESS,
          payload: { ...response, param: { ...xProps } },
        })
        if (onSuccess) { onSuccess() }
      })
      .catch((error) => {
        dispatch({
          type: ERROR,
          payload: error,
        })
        if (onError) onError(error)
      })
  }
}
