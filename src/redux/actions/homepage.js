import { getList } from '~/services/apis/gallery'
import { apiAction } from '../builder'

export const HOME_PAGE_ACTIONS = Object.freeze({
  GET_IMAGE_LIST_HOME_PAGE: 'homepage.GET_IMAGE_LIST_HOME_PAGE',
  GET_IMAGE_LIST_HOME_PAGE_OK: 'homepage.GET_IMAGE_LIST_HOME_PAGE_OK',
  GET_IMAGE_LIST_HOME_PAGE_ERR: 'homepage.GET_IMAGE_LIST_HOME_PAGE_ERR',
})

export const getGallery = (data, onSuccess, onError) => apiAction(
  getList(data),
  [
    HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE,
    HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE_OK,
    HOME_PAGE_ACTIONS.GET_IMAGE_LIST_HOME_PAGE_ERR,
  ],
  data,
  onSuccess,
  onError,
)

export default {
  // getImageList,
  getGallery,
}
