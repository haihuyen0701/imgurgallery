const HtmlWebpackPlugin = require('html-webpack-plugin')
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const CompressionPlugin = require('compression-webpack-plugin')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: `${__dirname}/src/index.js`,

  output: {
    path: `${__dirname}/public`,
    filename: '[name].[hash].js',
    publicPath: '/',
  },

  performance: {
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
      },
      {
        test: /\.(pdf)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './pdf/',
          },
        },
      },
      {
        test: /\.aac$/,
        loader: 'file-loader',
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'sass-loader',
          },
        ],
      },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      {
        loader: 'file-loader', test: /\.(ttf|eot|svg|gif|jpe?g|png|ico)$/,
      },
    ],
  },

  // resolve: {
  //   extensions: ['', '.js', '.jsx', '.css'],
  //   modulesDirectories: [
  //     'node_modules',
  //   ],
  // },

  externals: {},

  devServer: {
    historyApiFallback: true,
    inline: true,
    port: 8888,
  },

  optimization: {
    runtimeChunk: false,
    splitChunks: {
      cacheGroups: {
        default: false,
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
          reuseExistingChunk: true,
        },
      },
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      favicon: 'src/assets/favicon.ico',
      template: 'src/assets/index.html',
    }),
    new CopyPlugin({
      patterns: [
        { from: 'src/assets/img', to: 'img' },
        { from: 'src/assets/icons', to: 'icons' },
      ],
    }),
    new webpack.ContextReplacementPlugin(
      /moment[/\\]locale$/,
      /es-us|en-ca|vi/,
    ),
    new CompressionPlugin(),
  ],
}
